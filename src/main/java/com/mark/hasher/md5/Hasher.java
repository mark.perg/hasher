package com.mark.hasher.md5;

import java.nio.ByteBuffer;

public class Hasher {
    
    //MD5 Constants
    private final int WORD_A = 0x67452301;
    private final int WORD_B = 0xefcdab89;
    private final int WORD_C = 0x98badcfe;
    private final int WORD_D = 0x10325476;
    
    private int[] T = new int[64];
    
    public Hasher(){
        T = this.calculateT();
        
        /*
        System.out.println(WORD_A);
        System.out.println(WORD_B);
        System.out.println(WORD_C);
        System.out.println(WORD_D);
        
        for(int t : T){
            System.out.println(t);
        }
        */
        
    }
    
    public String hash(String input){
        byte[] stream = appendAndPrepare(input);
        
        /*
        for(byte b : stream){
            System.out.print(String.format("%8s", Integer.toBinaryString(b & 0xFF)).replace(' ','0'));
        }
        System.out.println();
        */
        int numberOfIterations = (stream.length * 8) / 512;
        
        
        int a = WORD_A;
        int b = WORD_B;
        int c = WORD_C;
        int d = WORD_D;
        
        for(int i = 0; i < numberOfIterations; i++){
            int aa = a;
            int bb = b;
            int cc = c;
            int dd = d;
            
            int[] x = new int[16];
            for(int j = 0; j < x.length; j++){
                byte[] temp = new byte[4];
                temp[0] = stream[(i * (512/8)) + (j * 4) + 0];
                temp[1] = stream[(i * (512/8)) + (j * 4) + 1];
                temp[2] = stream[(i * (512/8)) + (j * 4) + 2];
                temp[3] = stream[(i * (512/8)) + (j * 4) + 3];
                
                int val = bytesToInt(temp);
                
                byte[] temp2 = new byte[4];
                
                for (int k = 0; k < 4; k++)
                  {
                    temp2[k] = (byte)val;
                    val >>>= 8;
                  }
                x[j] = bytesToInt(temp2);
            }
            
            
            /* Do the following 16 operations on FF.
               a = b + ((a + F(b,c,d) + X[k] + T[i]) <<< s)
                [ABCD  0  7  1]  [DABC  1 12  2]  [CDAB  2 17  3]  [BCDA  3 22  4]
                [ABCD  4  7  5]  [DABC  5 12  6]  [CDAB  6 17  7]  [BCDA  7 22  8]
                [ABCD  8  7  9]  [DABC  9 12 10]  [CDAB 10 17 11]  [BCDA 11 22 12]
                [ABCD 12  7 13]  [DABC 13 12 14]  [CDAB 14 17 15]  [BCDA 15 22 16]
            
            */

                int val = FF(a, b, c, d, x[0],  T[0],  7);
                a = d;
                d = c;
                c = b;
                b = val;
                val = FF(a, b, c, d, x[1],  T[1], 12 );
                a = d;
                d = c;
                c = b;
                b = val;
                val = FF(a, b, c, d, x[2],  T[2], 17 );
                a = d;
                d = c;
                c = b;
                b = val;
                val = FF(a, b, c, d,  x[3],  T[3], 22 );   
                a = d;
                d = c;
                c = b;
                b = val;
                val = FF(a, b, c, d, x[4],  T[4],  7 );
                a = d;
                d = c;
                c = b;
                b = val;
                val = FF(a, b, c, d, x[5],  T[5], 12 );
                a = d;
                d = c;
                c = b;
                b = val;
                val = FF(a, b, c, d, x[6],  T[6], 17 );
                a = d;
                d = c;
                c = b;
                b = val;
                val = FF(a, b, c, d, x[7],  T[7], 22 );
                a = d;
                d = c;
                c = b;
                b = val;
                val = FF(a, b, c, d, x[8],  T[8],  7 );
                a = d;
                d = c;
                c = b;
                b = val;
                val = FF(a, b, c, d, x[9],  T[9], 12);
                a = d;
                d = c;
                c = b;
                b = val;
                val = FF(a, b, c, d, x[10], T[10], 17);
                a = d;
                d = c;
                c = b;
                b = val;
                val = FF(a, b, c, d, x[11], T[11], 22);
                a = d;
                d = c;
                c = b;
                b = val;
                val = FF(a, b, c, d, x[12], T[12],  7);
                a = d;
                d = c;
                c = b;
                b = val;
                val = FF(a, b, c, d, x[13], T[13], 12);
                a = d;
                d = c;
                c = b;
                b = val;
                val = FF(a, b, c, d, x[14], T[14], 17);
                a = d;
                d = c;
                c = b;
                b = val;
                val = FF(a, b, c, d, x[15], T[15], 22);
                a = d;
                d = c;
                c = b;
                b = val;
                
                
                
            
            /* Do the following 16 operations on GG.
                a = b + ((a + G(b,c,d) + X[k] + T[i]) <<< s). 
                [ABCD  1  5 17]  [DABC  6  9 18]  [CDAB 11 14 19]  [BCDA  0 20 20]
                [ABCD  5  5 21]  [DABC 10  9 22]  [CDAB 15 14 23]  [BCDA  4 20 24]
                [ABCD  9  5 25]  [DABC 14  9 26]  [CDAB  3 14 27]  [BCDA  8 20 28]
                [ABCD 13  5 29]  [DABC  2  9 30]  [CDAB  7 14 31]  [BCDA 12 20 32]
            
            */
           
                val = GG(a, b, c, d, x[1],  T[16],  5);
				a = d;
                d = c;
                c = b;
                b = val;
                val = GG(a, b, c, d, x[6],  T[17],  9);
				a = d;
                d = c;
                c = b;
                b = val;
    			val = GG(a, b, c, d, x[11], T[18], 14);
    			a = d;
                d = c;
                c = b;
                b = val;
				val = GG(a, b, c, d, x[0],  T[19], 20);
				a = d;
                d = c;
                c = b;
                b = val;
    			val = GG(a, b, c, d, x[5],  T[20],  5);
				a = d;
                d = c;
                c = b;
                b = val;
    			val = GG(a, b, c, d, x[10], T[21],  9);
				a = d;
                d = c;
                c = b;
                b = val;
    			val = GG(a, b, c, d, x[15], T[22], 14);
				a = d;
                d = c;
                c = b;
                b = val;
    			val = GG(a, b, c, d, x[4],  T[23], 20);
				a = d;
                d = c;
                c = b;
                b = val;
    			val = GG(a, b, c, d, x[9],  T[24],  5);
				a = d;
                d = c;
                c = b;
                b = val;
    			val = GG(a, b, c, d, x[14], T[25],  9);
				a = d;
                d = c;
                c = b;
                b = val;
    			val = GG(a, b, c, d, x[3],  T[26], 14);
				a = d;
                d = c;
                c = b;
                b = val;
    			val = GG(a, b, c, d, x[8],  T[27], 20);
				a = d;
                d = c;
                c = b;
                b = val;
                val = GG(a, b, c, d, x[13], T[28],  5);
				a = d;
                d = c;
                c = b;
                b = val;
                val = GG(a, b, c, d, x[2],  T[29],  9);
				a = d;
                d = c;
                c = b;
                b = val;
                val = GG(a, b, c, d, x[7],  T[30], 14);
				a = d;
                d = c;
                c = b;
                b = val;
                val = GG(a, b, c, d, x[12], T[31], 20);
				a = d;
                d = c;
                c = b;
                b = val;
                
                
            
            
            
            /* Do the following 16 operations on HH.
                a = b + ((a + H(b,c,d) + X[k] + T[i]) <<< s)
                [ABCD  5  4 33]  [DABC  8 11 34]  [CDAB 11 16 35]  [BCDA 14 23 36]
                [ABCD  1  4 37]  [DABC  4 11 38]  [CDAB  7 16 39]  [BCDA 10 23 40]
                [ABCD 13  4 41]  [DABC  0 11 42]  [CDAB  3 16 43]  [BCDA  6 23 44]
                [ABCD  9  4 45]  [DABC 12 11 46]  [CDAB 15 16 47]  [BCDA  2 23 48]
            
            */
           
                
                val = HH(a, b, c, d, x[5],  T[32],  4);
				a = d;
                d = c;
                c = b;
                b = val;
    			val = HH(a, b, c, d, x[8],  T[33], 11);
				a = d;
                d = c;
                c = b;
                b = val;				
    			val = HH(a, b, c, d, x[11], T[34], 16);
				a = d;
                d = c;
                c = b;
                b = val;				
    			val = HH(a, b, c, d, x[14], T[35], 23);
				a = d;
                d = c;
                c = b;
                b = val;				
    			val = HH(a, b, c, d, x[1],  T[36],  4);
				a = d;
                d = c;
                c = b;
                b = val;				
    			val = HH(a, b, c, d, x[4],  T[37], 11);
				a = d;
                d = c;
                c = b;
                b = val;				
    			val = HH(a, b, c, d, x[7],  T[38], 16);
				a = d;
                d = c;
                c = b;
                b = val;		
    			val = HH(a, b, c, d, x[10], T[39], 23);
				a = d;
                d = c;
                c = b;
                b = val;				
    			val = HH(a, b, c, d, x[13], T[40],  4);
				a = d;
                d = c;
                c = b;
                b = val;				
    			val = HH(a, b, c, d, x[0],  T[41], 11);
				a = d;
                d = c;
                c = b;
                b = val;				
    			val = HH(a, b, c, d, x[3],  T[42], 16);
				a = d;
                d = c;
                c = b;
                b = val;				
                val = HH(a, b, c, d, x[6],  T[43], 23);
				a = d;
                d = c;
                c = b;
                b = val;				
                val = HH(a, b, c, d, x[9],  T[44],  4);
				a = d;
                d = c;
                c = b;
                b = val;				
                val = HH(a, b, c, d, x[12], T[45], 11);
				a = d;
                d = c;
                c = b;
                b = val;				
                val = HH(a, b, c, d, x[15], T[46], 16);
				a = d;
                d = c;
                c = b;
                b = val;				
                val = HH(a, b, c, d, x[2],  T[47], 23);
				a = d;
                d = c;
                c = b;
                b = val;		
                

            /* Do the following 16 operations on II.
                a = b + ((a + I(b,c,d) + X[k] + T[i]) <<< s).
                [ABCD  0  6 49]  [DABC  7 10 50]  [CDAB 14 15 51]  [BCDA  5 21 52]
                [ABCD 12  6 53]  [DABC  3 10 54]  [CDAB 10 15 55]  [BCDA  1 21 56]
                [ABCD  8  6 57]  [DABC 15 10 58]  [CDAB  6 15 59]  [BCDA 13 21 60]
                [ABCD  4  6 61]  [DABC 11 10 62]  [CDAB  2 15 63]  [BCDA  9 21 64]
            */
           
               	val = II(a, b, c, d, x[0],  T[48],  6);
				a = d;
                d = c;
                c = b;
                b = val;				
    			val = II(a, b, c, d, x[7],  T[49], 10);
				a = d;
                d = c;
                c = b;
                b = val;				
    			val = II(a, b, c, d, x[14], T[50], 15);
				a = d;
                d = c;
                c = b;
                b = val;				
    			val = II(a, b, c, d, x[5],  T[51], 21);
				a = d;
                d = c;
                c = b;
                b = val;				
    			val = II(a, b, c, d, x[12], T[52],  6);
				a = d;
                d = c;
                c = b;
                b = val;				
    			val = II(a, b, c, d, x[3],  T[53], 10);
				a = d;
                d = c;
                c = b;
                b = val;				
    			val = II(a, b, c, d, x[10], T[54], 15);
				a = d;
                d = c;
                c = b;
                b = val;				
    			val = II(a, b, c, d, x[1],  T[55], 21);
				a = d;
                d = c;
                c = b;
                b = val;				
    			val = II(a, b, c, d, x[8],  T[56],  6);
				a = d;
                d = c;
                c = b;
                b = val;				
    			val = II(a, b, c, d, x[15], T[57], 10);
				a = d;
                d = c;
                c = b;
                b = val;				
    			val = II(a, b, c, d, x[6],  T[58], 15);
				a = d;
                d = c;
                c = b;
                b = val;				
    			val = II(a, b, c, d, x[13], T[59], 21);
				a = d;
                d = c;
                c = b;
                b = val;				
                val = II(a, b, c, d, x[4],  T[60],  6);
				a = d;
                d = c;
                c = b;
                b = val;				
                val = II(a, b, c, d, x[11], T[61], 10);
				a = d;
                d = c;
                c = b;
                b = val;				
                val = II(a, b, c, d, x[2],  T[62], 15);
				a = d;
                d = c;
                c = b;
                b = val;				
                val = II(a, b, c, d, x[9],  T[63], 21);
				a = d;
                d = c;
                c = b;
                b = val;	
            
            
            a = moduloSum(a, aa);
            b = moduloSum(b, bb);
            c = moduloSum(c, cc);
            d = moduloSum(d, dd);
            

        }
        
        byte[] md5 = new byte[16];
        int count = 0;
        for (int i = 0; i < 4; i++)
        {
          int n = (i == 0) ? a : ((i == 1) ? b : ((i == 2) ? c : d));
          for (int j = 0; j < 4; j++)
          {
            md5[count++] = (byte)n;
            n >>>= 8;
          }
        }
       
        StringBuilder sb = new StringBuilder();
        for (byte t : md5) {
            sb.append(String.format("%02X", t));
        }
        
        return sb.toString();

        
    }
    
    private byte[] appendAndPrepare(String input){
        byte[] content = input.getBytes();
        ByteBuffer b = ByteBuffer.allocate(Long.BYTES);
        b.putLong(8 * content.length);
        byte[] size =  b.array();
        
        int bytesToAdd = 0;
        
        if(((8 * content.length) % 512)  < 448 ){
            bytesToAdd = (448 - ((8 * content.length) % 512)) / 8;
        } else {
            bytesToAdd = ((512 - (((8 * content.length) % 512) - 448)) / 8); 
        }
        
        byte[] append = new byte[bytesToAdd];
        for(int i = 0; i < bytesToAdd; i++){
            if(i == 0){
                append[i] = (byte)0x80;
            } else {
                append[i] = (byte)0x00;
            }
            
        }
        
        
        /*
        long messageLenBits = (long)content.length << 3;
    
        for (int i = 0; i < 8; i++)
        {
          size[i] = (byte)messageLenBits;
          messageLenBits >>>= 8;
        }
        */
       
       long messageLength = (long)(content.length * 8);
       byte[] fixedSize = new byte[8];
       
       for(int i = 0; i < 8; i++){
            fixedSize[i] = (byte)messageLength;
            messageLength = messageLength >>> 8;
       }
       
      return concatinateArray(concatinateArray(content, append), fixedSize);
        
    }
    
    private byte[] concatinateArray(byte[] a, byte[] b){
        byte[] result = new byte[a.length + b.length];
        
        for(int i = 0; i < a.length; i++){
            result[i] = a[i];
        }
        
        for(int j = a.length; j < a.length + b.length; j++){
            result[j] = b[j - a.length];
        }
        
        return result;
    }
    
    //Utility functions
    private int FF(int a, int b, int c, int d, int x, int t, int s){
        //a = b + ((a + F(b,c,d) + X[k] + T[i]) <<< s)
        return moduloSum(b, leftRotation(moduloSum(moduloSum(moduloSum(a, F(b, c, d)), x), t),s));
    }
    
    private int GG(int a, int b, int c, int d, int x, int t, int s){
        //a = b + ((a + G(b,c,d) + X[k] + T[i]) <<< s)
        return moduloSum(b, leftRotation(moduloSum(moduloSum(moduloSum(a, G(b, c, d)), x), t), s));
    }
    
    private int HH(int a, int b, int c, int d, int x, int t, int s){
        //a = b + ((a + H(b,c,d) + X[k] + T[i]) <<< s)
        return moduloSum(b, leftRotation(moduloSum(moduloSum(moduloSum(a, H(b, c, d)), x), t), s));
    }
    
    private int II(int a, int b, int c, int d, int x, int t, int s){
        //a = b + ((a + I(b,c,d) + X[k] + T[i]) <<< s)
        return moduloSum(b, leftRotation(moduloSum(moduloSum(moduloSum(a, I(b, c, d)), x), t), s));
    }
    
    private int F(int x, int y, int z){
        //F(X,Y,Z) = XY v not(X) Z
        int f = ((x & y) | ((~x) & z));
        return f;
    }
    private int G(int x, int y, int z){
        //G(X,Y,Z) = XZ v Y not(Z)
        return ((x & z) | (y & (~z)));
    }
    private int H(int x, int y, int z){
        //H(X,Y,Z) = X xor Y xor Z
        return (x ^ y ^ z);
    }
    private int I(int x, int y, int z){
        //I(X,Y,Z) = Y xor (X v not(Z))
        return (y ^ (x | (~z)));
    }
    
    
    private int leftRotation(int val,  int s){
        int res = val;
        for(int i = 0; i < s; i++){
            res = (res << 1) | (res >>> (Integer.SIZE - 1));
        }
        return res;
        
    }
              
          
          
          
    //Helper functions
    
    public int bytesToInt(byte[] bytes){

        int res = 0;
        for (int i=0; i<4; i++) {
            res <<= 8;
            res |= (int)bytes[i] & 0xFF;
        }
        return res;
    }

 
    
    
    private int[] calculateT(){
        int[] res = new int[64];

        for(int i = 0 ; i < 64; i++){
            long val = (long)(4294967296L * Math.abs(Math.sin((double)i + 1)));
            res[i] = convertUnsignedIntToInt(val);
        }
        return res;
    }
    
    private int moduloSum(int a, int b){
        long sum = convertIntToUnsignedInt(a) + convertIntToUnsignedInt(b);
        if(sum > 4294967296L){
            return convertUnsignedIntToInt(sum - 4294967296L);
        } else {
            return convertUnsignedIntToInt(sum);
        }
        
    }
    
    private long convertIntToUnsignedInt(int intValue){
        ByteBuffer buf = ByteBuffer.allocate(Long.BYTES);
        buf.putInt(Long.BYTES - Integer.BYTES, intValue);
        return buf.getLong();
    }
    
    private int convertUnsignedIntToInt(long intValue){
        ByteBuffer buff = ByteBuffer.allocate(Long.BYTES);
        buff.putLong(intValue);
        return buff.getInt(4);        
    }
    
    
}
