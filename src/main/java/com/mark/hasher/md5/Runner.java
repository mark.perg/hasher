package com.mark.hasher.md5;

public class Runner {
    
    public static void main(String[] args){

        System.out.println("Hasher");
        Hasher h = new Hasher();
        
        System.out.println(h.hash("")); //d41d8cd98f00b204e9800998ecf8427e
        System.out.println(h.hash("a")); //0cc175b9c0f1b6a831c399e269772661
        System.out.println(h.hash("abc")); //900150983cd24fb0d6963f7d28e17f72
        System.out.println(h.hash("message digest")); //f96b697d7cb7938d525a2f31aaf161d0
        System.out.println(h.hash("abcdefghijklmnopqrstuvwxyz")); //c3fcd3d76192e4007dfb496cca67e13b
        System.out.println(h.hash("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789")); //d174ab98d277d9f5a5611c2c9f419d9f
        System.out.println(h.hash("12345678901234567890123456789012345678901234567890123456789012345678901234567890")); //57edf4a22be3c955ac49da2e2107b67a
        
    }
}
